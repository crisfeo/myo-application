#pragma once
#include "GL.h"
#include <vector>
#include <fstream>

using namespace std;

class ShaderProgram
{
private:
	vector<GLuint> ShaderIds;
	GLuint ProgramId;

	void CleanupProgram();
	bool LoadFileContents(char* filepath, vector<char> &buffer);
	void PrintShaderInfoLog(GLuint shaderId);
	void PrintProgramInfoLog();

public:
	ShaderProgram();
	~ShaderProgram();

	GLuint AddShader(char* filename, GLenum type);
	int LinkProgram();
	void BindAttribute(GLuint shaderId, GLuint location, char* name);
	GLuint GetUniform(char* name);
};

