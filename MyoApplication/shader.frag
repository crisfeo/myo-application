uniform vec2 dimensions;

void main()
{
	gl_FragColor = vec4(gl_FragCoord.x / dimensions.x, 0.0, 0.0, 1.0);
}