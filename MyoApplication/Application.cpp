#include "Application.h"

#define WINDOW_TITLE "Myo Application"
#define VERTEX_SHADER_ATTRIBUTE 0
#define VERTEX_COUNT 4
const float VERTEX_DATA[4][3] = {
	{ -1.f, -1.f, 0.f },
	{  1.f, -1.f, 0.f },
	{  1.f,  1.f, 0.f },
	{ -1.f,  1.f, 0.f }
};


#pragma region Constructors/Destructors

Application::Application()
{
	if (Application::Instance != NULL)
		throw new exception("Only one Application instance is allowed!");
	Application::Instance = this;

	InitializeGLFW();
	InitializeGLEW();
	InitializeShaders();
	InitializeUniforms();
	InitializeVBO();

	while (!glfwWindowShouldClose(AppWindow))
		Render();

	Quit(EXIT_SUCCESS);
}

Application* Application::Instance = NULL;
Application* Application::GetInstance()
{
	if (Application::Instance == NULL)
		new Application();
	return Application::Instance;
}


Application::~Application()
{
	Quit(EXIT_SUCCESS);
}


void Application::Quit(int code)
{
	Application* instance = GetInstance();
	delete instance->Shaders;
	if (instance->AppWindow)
		glfwDestroyWindow(instance->AppWindow);
	glfwTerminate();
	exit(code);
}

#pragma endregion

#pragma region Initializers

void Application::InitializeGLFW()
{
	glfwSetErrorCallback(OnError);
	glfwInit();
	AppWindow = glfwCreateWindow(640, 480, WINDOW_TITLE, NULL, NULL);
	glfwMakeContextCurrent(AppWindow);
	glfwSetWindowSizeCallback(AppWindow, OnResize);
	glfwSetKeyCallback(AppWindow, OnKey);
}


void Application::InitializeGLEW()
{
	if (glewInit() != GLEW_OK)
	{
		printf("GLEW failed to initialize!");
		Quit(EXIT_FAILURE);
	}
}


void Application::InitializeShaders()
{
	Shaders = new ShaderProgram();
	GLuint vertShader = Shaders->AddShader("shader.vert", GL_VERTEX_SHADER);
	GLuint fragShader = Shaders->AddShader("shader.frag", GL_FRAGMENT_SHADER);
	Shaders->BindAttribute(vertShader, VERTEX_SHADER_ATTRIBUTE, "in_Position");

	if (!Shaders->LinkProgram())
	{
		printf("Could not link shader program!");
		Quit(EXIT_FAILURE);
	}
}


void Application::InitializeUniforms()
{
	UniformDimensions = Shaders->GetUniform("dimensions");
	int width, height;
	glfwGetWindowSize(AppWindow, &width, &height);
	glUniform2f(UniformDimensions, (float)width, (float)height);
}


void Application::InitializeVBO()
{
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, VERTEX_COUNT * 3 * sizeof(float), VERTEX_DATA, GL_STATIC_DRAW);
	glVertexAttribPointer(VERTEX_SHADER_ATTRIBUTE, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(VERTEX_SHADER_ATTRIBUTE);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
}

#pragma endregion

#pragma region Event Handlers

void Application::Render()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glDrawArrays(GL_TRIANGLE_FAN, 0, VERTEX_COUNT);
	glfwSwapBuffers(AppWindow);
	glfwPollEvents();
}


void Application::OnError(int error, const char* description)
{
	Application* instance = GetInstance();

	printf("GLFW Error! Code: %i. Description: %s", error, description);
	instance->Quit(EXIT_FAILURE);
}


void Application::OnResize(GLFWwindow* window, int width, int height)
{
	Application* instance = GetInstance();

	glViewport(0, 0, width, height);
	glUniform2f(instance->UniformDimensions, (float)width, (float)height);
}


void Application::OnKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	Application* instance = GetInstance();

	if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE)
		glfwSetWindowShouldClose(instance->AppWindow, true);
}


#pragma endregion