#pragma once
#include <stdlib.h>
#include <stdio.h>
#include "GL.h"
#include "ShaderProgram.h"

using namespace std;

class Application
{
private:
	static Application* Instance;

	GLFWwindow* AppWindow;
	ShaderProgram* Shaders;

	GLuint UniformDimensions;

	void InitializeGLFW();
	void InitializeGLEW();
	void InitializeShaders();
	void InitializeUniforms();
	void InitializeVBO();

	void Render();
	static void OnError(int error, const char* description);
	static void OnResize(GLFWwindow* window, int width, int height);
	static void OnKey(GLFWwindow* window, int key, int scancode, int action, int mods);

	

public:
	Application();
	~Application();
	
	static Application* GetInstance();
	static void Quit(int code);
};

