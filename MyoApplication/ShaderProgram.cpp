#include "ShaderProgram.h"

#pragma region Constructor/Destructor

ShaderProgram::ShaderProgram()
{
	ProgramId = glCreateProgram();
}


ShaderProgram::~ShaderProgram()
{
	CleanupProgram();
}


void ShaderProgram::CleanupProgram()
{
	if (ProgramId)
		glDeleteProgram(ProgramId);

	for (auto it = begin(ShaderIds); it != end(ShaderIds); it++)
		glDeleteShader(*it);

	ShaderIds.clear();
}

#pragma endregion

#pragma region Methods

GLuint ShaderProgram::AddShader(char* filename, GLenum type)
{
	GLuint newShaderId = glCreateShader(type);
	if (newShaderId)
	{
		ShaderIds.push_back(newShaderId);
		vector<char> shaderCode;
		LoadFileContents(filename, shaderCode);
		char* shaderCodeChars = &(shaderCode[0]);
		glShaderSource(newShaderId, 1, (const GLchar**)&shaderCodeChars, NULL);
		int compiled = GL_FALSE;
		glCompileShader(newShaderId);
		glGetShaderiv(newShaderId, GL_COMPILE_STATUS, &compiled);
		if (!compiled)
			PrintShaderInfoLog(newShaderId);
	}
	return newShaderId;
}


int ShaderProgram::LinkProgram()
{
	int linked = GL_FALSE;
	if (ProgramId)
	{
		for (auto it = begin(ShaderIds); it != end(ShaderIds); it++)
			glAttachShader(ProgramId, *it);
		glLinkProgram(ProgramId);
		glGetProgramiv(ProgramId, GL_LINK_STATUS, &linked);
		if (linked)
			glUseProgram(ProgramId);
		else
			PrintProgramInfoLog();
	}
	return linked;
}


void ShaderProgram::BindAttribute(GLuint shaderId, GLuint location, char* name)
{
	glBindAttribLocation(shaderId, location, name);
}

GLuint ShaderProgram::GetUniform(char* name)
{
	return glGetUniformLocation(ProgramId, name);
}


bool ShaderProgram::LoadFileContents(char* filepath, vector<char> &buffer)
{
	ifstream file;
	file.open(filepath);
	if (file.is_open())
	{
		while (!file.eof())
		{
			buffer.push_back(file.get());
		}
		buffer.push_back('\0');
		file.close();
		return true;
	}
	return false;
}


void ShaderProgram::PrintShaderInfoLog(GLuint shaderId)
{
	GLint logLength = 0;
	glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logLength);
	char* log = new char[logLength];
	glGetShaderInfoLog(shaderId, logLength, &logLength, log);
	printf("Shader compilation error:\n%s", log);
	delete[] log;
}

void ShaderProgram::PrintProgramInfoLog()
{
	GLint logLength = 0;
	glGetProgramiv(ProgramId, GL_INFO_LOG_LENGTH, &logLength);
	char* log = new char[logLength];
	glGetProgramInfoLog(ProgramId, logLength, &logLength, log);
	printf("Shader link error:\n%s", log);
	delete[] log;
}

#pragma endregion